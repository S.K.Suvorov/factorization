﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;

public class Factorization : MonoBehaviour
{
    public InputField inputField;
    private List<int> listOfPrimeNumber;
    public Text resultOfFactorization;
    // Start is called before the first frame update
  private  void Start()
    {
        listOfPrimeNumber = new List<int>();
        ListOfPrimeNumber();
       
    }


    void ListOfPrimeNumber()
    {//Данный метод используется для создания спсика простых чисел
        bool isPrime;
        for (int i = 2; i <= 10000; i++)
        {
            isPrime = true;
            for (int j = 2; j < i; j++)
            {
                if (i % j == 0)
                {
                    isPrime = false;
                    j = i;
                }
            }
            if (isPrime)
                listOfPrimeNumber.Add(i);

        }
    }
   public void OnFactorizationButtonPress()
    {//Данный метод срабатывает при нажатии на кнопку "Press for Factorization", к которому и прикрепляется.
        int number = Int32.Parse(inputField.text);

        bool isFactorization=false;
        for (int i = 0; i < listOfPrimeNumber.Count; i++)
        {
            if ((number % listOfPrimeNumber[i] == 0)
                && (listOfPrimeNumber.Contains(number / listOfPrimeNumber[i])))
            {
                isFactorization = true;
                resultOfFactorization.text = (number + "=" + listOfPrimeNumber[i] + "*" + number / listOfPrimeNumber[i]);
                i = listOfPrimeNumber.Count;
            }
        }
        if (!isFactorization)
           resultOfFactorization.text="Factorization is not possible";

    }
}

